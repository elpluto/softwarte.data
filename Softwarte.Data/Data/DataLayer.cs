﻿/************************************************
 *	Generic data layer for EF code first.			
 *	Programmed by: Rafael Hernández
 *	Revision Date: 13/04/2014
 *	Version: 1.2										
 * **********************************************/

namespace Softwarte.Data
{
  using Softwarte.Data.Common;
  using System;
  using System.Collections.Generic;
  using System.Data.Entity;
  using System.Linq;
  using System.Linq.Expressions;
  public class DataLayer<TContext> : IDataLayer, IDisposable where TContext : DbContext, new()
  {
    private readonly TContext _context;

    #region Ctor
    /// <summary>
    /// Ctor, by default:
    /// validationOnSave = true,
    /// autoDetectChanges = true,
    /// proxyCreatin = true,
    /// lazyLoading = true
    /// Tracking disabled = true; add AsNoTracking() to dbQuery.
    /// </summary>
    public DataLayer(bool validateOnSave = true, bool autoDetectChanges = true, bool proxyCreation = false, bool lazyLoading = false, bool trackingDisabled = true)
    {
      _context = new TContext();
      _context.Configuration.ValidateOnSaveEnabled = validateOnSave;
      _context.Configuration.AutoDetectChangesEnabled = autoDetectChanges;
      _context.Configuration.ProxyCreationEnabled = proxyCreation;
      _context.Configuration.LazyLoadingEnabled = lazyLoading;
      //
      this.TrackingDisabled = trackingDisabled;
    }
    #endregion

    #region Configuration properties
    public bool ValidateOnSaveEnabled
    {
      get { return _context.Configuration.ValidateOnSaveEnabled; }
      set { _context.Configuration.ValidateOnSaveEnabled = value; }
    }
    public bool AutoDetectChangesEnabled
    {
      get { return _context.Configuration.AutoDetectChangesEnabled; }
      set { _context.Configuration.AutoDetectChangesEnabled = value; }
    }
    public bool ProxyCreationEnabled
    {
      get { return _context.Configuration.ProxyCreationEnabled; }
      set { _context.Configuration.ProxyCreationEnabled = value; }
    }
    public bool LazyLoadingEnabled
    {
      get { return _context.Configuration.LazyLoadingEnabled; }
      set { _context.Configuration.LazyLoadingEnabled = value; }
    }
    public bool TrackingDisabled { get; set; }
    #endregion

    #region IDataLayer Members
    /// <summary>
    /// Build a queryable over an entity an configure it.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <returns></returns>
    private IQueryable<TEntity> BuildQuery<TEntity>() where TEntity : class, new()
    {
      if (TrackingDisabled)
      {
        return _context.Set<TEntity>().OfType<TEntity>().AsNoTracking();
      }
      else
      {
        return _context.Set<TEntity>().OfType<TEntity>();
      }
    }

    /// <summary>
    /// Get all items of an entity without count.It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetAll<TEntity>(params string[] eagerLoadEntities) where TEntity : class, new()
    {
      var query = BuildQuery<TEntity>();

      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Get all items of an entity with count.It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="totalCount"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetAll<TEntity>(out long totalCount, params string[] eagerLoadEntities) where TEntity : class, new()
    {
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Count.
      totalCount = GetCountAll<TEntity>(); //Get a count.
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Paginated query of all items of an entity with count.It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    /// <param name="totalCount"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetAll<TEntity>(int pageIndex, int pageSize, out long totalCount, params string[] eagerLoadEntities)
        where TEntity : class, new()
    {
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter
      query = query
               .Skip(pageIndex * pageSize)
               .Take(pageSize);
      //Count.
      totalCount = GetCountAll<TEntity>(); //Get count without pagination.
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Paginated get of all items of an entity without count.It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetAll<TEntity>(int pageIndex, int pageSize, params string[] eagerLoadEntities)
      where TEntity : class, new()
    {
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter
      query = query
               .Skip(pageIndex * pageSize)
               .Take(pageSize);
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Get count over all items of an entity.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <returns></returns>
    public long GetCountAll<TEntity>() where TEntity : class, new()
    {
      return BuildQuery<TEntity>().Count();
    }

    /// <summary>
    /// Paginated predicate based query of items of an entity with count and order. It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    /// <param name="order"></param>
    /// <param name="totalCount"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate, int pageIndex, int pageSize, OrderClause order, out long totalCount, params string[] eagerLoadEntities)
      where TEntity : class, new()
    {
      //Count.
      totalCount = GetCountByQuery<TEntity>(predicate); //Get count by predicate.
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter.
      query = query.Where(predicate);
      //Order.
      if (order.Direction == OrderDirectionEnum.Ascending)
      {
        query = query.OrderBy(c => order.FieldName);
      }
      else
      {
        query = query.OrderByDescending(c => order.FieldName);
      }
      //Pagination.
      query = query
                .Skip(pageIndex * pageSize)
                .Take(pageSize);
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Paginated and predicate based query of items of an entity without count but with order. It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <param name="pageIndex"></param>
    /// <param name="pageSize"></param>
    /// <param name="order"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate, int pageIndex, int pageSize, OrderClause order, params string[] eagerLoadEntities)
      where TEntity : class, new()
    {
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter.
      query = query.Where(predicate);
      //Order.
      if (order.Direction == OrderDirectionEnum.Ascending)
      {
        query = query.OrderBy(c => order.FieldName);
      }
      else
      {
        query = query.OrderByDescending(c => order.FieldName);
      }
      //Pagination.
      query = query
                .Skip(pageIndex * pageSize)
                .Take(pageSize);
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Predicate based query of all items of an entity with order. It can accept, optionally, the payload associated with the results..
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <param name="order"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate, OrderClause order, params string[] eagerLoadEntities) where TEntity : class, new()
    {
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter.
      query = query.Where(predicate);
      //Order.
      if (order.Direction == OrderDirectionEnum.Ascending)
      {
        query = query.OrderBy(c => order.FieldName);
      }
      else
      {
        query = query.OrderByDescending(c => order.FieldName);
      }
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Predicate based query with order and count. It can accept, optionally, the payload associated with the results.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <param name="totalCount"></param>
    /// <param name="order"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public ICollection<TEntity> GetByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate, out long totalCount, OrderClause order, params string[] eagerLoadEntities) where TEntity : class, new()
    {
      //Count.
      totalCount = GetCountByQuery<TEntity>(predicate); //Get count by predicate.
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter.
      query = query.Where(predicate);
      //Order.
      if (order.Direction == OrderDirectionEnum.Ascending)
      {
        query = query.OrderBy(c => order.FieldName);
      }
      else
      {
        query = query.OrderByDescending(c => order.FieldName);
      }
      //Results.
      return query.ToList();
    }

    /// <summary>
    /// Get a items count of an entity with a predicate based query.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <returns></returns>
    public long GetCountByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, new()
    {
      return BuildQuery<TEntity>().Where(predicate).Count();
    }

    /// <summary>
    /// Return an entity using a predicate based query.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="predicate"></param>
    /// <param name="eagerLoadEntities"></param>
    /// <returns></returns>
    public TEntity GetEntityByQuery<TEntity>(Expression<Func<TEntity, bool>> predicate,
                                params string[] eagerLoadEntities) where TEntity : class, new()
    {
      //Query.
      var query = BuildQuery<TEntity>();
      //Payload.
      query = eagerLoadEntities.Aggregate(query, (current, related) => current.Include(related));
      //Filter
      query = query.Where(predicate);
      //Result, devuelve nulo si no existe. Otra opción es generar exception mediante Single o First.
      return query.FirstOrDefault();
    }

    /// <summary>
    /// Generic method to save an entity. Entity is saved although hasn't changes. 
    /// Note that entity must exist in the exiting context. If not you should attach first or use another method for save out of context.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="entity"></param>
    /// <returns></returns>
    public bool SaveEntity<TEntity>(TEntity entity, bool forceDetectChanges = true)
      where TEntity : class, new()
    {
      /*
       * enableDetectingChanges = true overwrite the AutoDetectChanges configuration.
       * enableDetectingChanges = false need change AutoDetectChanges in the caller.
       */
      //
      var currenAutoDetectChangesConfigValue = this.AutoDetectChangesEnabled;
      //Force to true.
      if (forceDetectChanges) this.AutoDetectChangesEnabled = true;
      _context.Entry<TEntity>(entity).State = EntityState.Modified;
      var result = _context.SaveChanges();
      //Restore current config value.
      this.AutoDetectChangesEnabled = currenAutoDetectChangesConfigValue;
      return true;
    }
    /// <summary>
    /// Remove an entity.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="entity"></param>
    /// <returns></returns>
    public bool DeleteEntity<TEntity>(TEntity entity) where TEntity : class, new()
    {
      _context.Set<TEntity>().Attach(entity);
      _context.Set<TEntity>().Remove(entity);
      _context.SaveChanges();
      return true;
    }
    /// <summary>
    /// Execute a sql code directly to database.
    /// </summary>
    /// <param name="sqlCommand"></param>
    /// <param name="async"></param>
    /// <returns></returns>
    public bool ExecuteSQL(bool async, string sqlCommand, params object[] parameters)
    {
      if (async)
      {
        _context.Database.ExecuteSqlCommandAsync(sqlCommand, parameters);
      }
      else
      {
        _context.Database.ExecuteSqlCommand(sqlCommand, parameters);
      }
      return true;
    }
    /// <summary>
    /// Generic method to create an entity with persistence.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <param name="entity"></param>
    /// <returns></returns>
    public TEntity CreateEntity<TEntity>(TEntity entity)
      where TEntity : class, new()
    {
      _context.Set<TEntity>().Add(entity);
      _context.SaveChanges();
      return entity;
    }
    #endregion

    #region IDisposable Members
    /// <summary>
    /// Dispose resources and context.
    /// </summary>
    public void Dispose()
    {
      _context.Dispose();
    }

    #endregion
  }
}


